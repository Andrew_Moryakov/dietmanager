﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Awesomium.Core;
using Awesomium.Windows.Controls;
using Microsoft.Win32;

namespace DietManagerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
    /// 
    public partial class MainWindow : Window
	{

        System.Windows.Threading.DispatcherTimer timer;
        public MainWindow()
		{
            timer  = new DispatcherTimer();
            timer.Tick += Timer_Tick;
            timer.Interval =  TimeSpan.FromMilliseconds(800);

            InitializeComponent();
            WebControl1.MouseMove += WebControl1_MouseMove;
            WebControl1.LoadingFrameComplete += LoadingFramecompleted;
            this.WebControl1.Source = new Uri(@"C:\Users\Andrew_Moryakov\Desktop\Clone\portfolio\index.html", UriKind.Relative);

            
		    //WebSession s = WebCore.CreateWebSession(new WebPreferences());
		    //WebView vw = WebCore.CreateWebView(1024, 768, s);

		    //vw.Source = new Uri("https://www.google.ru");
		    //while (!vw.IsDocumentReady && vw.HTML == "" && vw.IsLoading)// Тут поставил много проверок, так как одна иногда не срабатывает.
		    //{ 
		    //	WebCore.Update();// VS ругается и говорит, что метод уже устарел, но он работает.
		    //}
		}

        private void WebControl1_MouseMove(object sender, MouseEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void WebControl1_MouseLeftButtonUp1(object sender, MouseButtonEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void WebControl1_KeyDown(object sender, KeyEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void WebControl1_ManipulationStarting(object sender, ManipulationStartingEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void WebControl1_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void WebControl1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (WebControl1.IsDocumentReady)
            {
                string btn2 =

                    WebControl1.ExecuteJavascriptWithResult("callback").ToString();
                if (btn2 == "ok")
                {
                    new OpenFileDialog().ShowDialog();
                    WebControl1.ExecuteJavascript("callback = null");
                }
            }
        }

        public void LoadingFramecompleted(object sender, FrameEventArgs e)
        {
          //timer.Start();
           // Task.Factory.StartNew(TaskMethod);
        }

        private void WebControl1_DocumentReady(object sender, DocumentReadyEventArgs e)
        {

           
        }

	    private void TaskMethod()
	    {
	        while (true)
	        {
                Task.Delay(TimeSpan.FromMilliseconds(500)).Wait();
                App.Current.Dispatcher.Invoke(() =>
                {

                    if (WebControl1.IsDocumentReady)
	                {
                        string btn2 =

	                        WebControl1.ExecuteJavascriptWithResult("callback").ToString();
	                    if (btn2 == "ok")
	                    {
	                        new OpenFileDialog().ShowDialog();
                            WebControl1.ExecuteJavascript("callback = null");
                        }
	                }

	            });
	        }
	    }




	    void webView_NativeViewInitialized(object sender, Awesomium.Core.WebViewEventArgs e)
		{
            
            //  this.WebControl1.Source = new Uri(@"C:\Users\Andrew_Moryakov\Desktop\Clone\portfolio\index.html", UriKind.Relative);

            //WebControl1.Source = @"C:\Users\Andrew_Moryakov\Desktop\Clone\portfolio\index.html".ToUri();
            // WebControl1.LoadHTML(File.ReadAllText(@"C:\Users\Andrew_Moryakov\Desktop\Clone\portfolio\index.html"));
            //WebControl1.Source = "";new Uri($"data:text/html,{page1}", UriKind.Absolute);
            // WebControl1.LoadHTML(page1);
            //WebControl1.Focus();
        }
	}
}
